package com.example.order.client.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.UUID;


@Data
@AllArgsConstructor
@NoArgsConstructor
public class OrchestratorRequestDTO {
  private Integer userId;
  private Integer productId;
  private UUID orderId;
  private Double amount;
}
