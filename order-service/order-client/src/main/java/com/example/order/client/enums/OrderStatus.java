package com.example.order.client.enums;

public enum OrderStatus {
  ORDER_CREATED,
  ORDER_CANCELLED,
  ORDER_COMPLETED
}
