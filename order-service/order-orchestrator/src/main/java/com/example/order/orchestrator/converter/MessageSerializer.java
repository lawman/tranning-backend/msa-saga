package com.example.order.orchestrator.converter;

import com.example.order.client.dto.MessageKafka;
import com.example.order.client.dto.OrchestratorRequestDTO;
import com.example.order.client.dto.OrchestratorResponseDTO;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.apache.kafka.common.errors.SerializationException;
import org.apache.kafka.common.serialization.Serializer;

public class MessageSerializer implements Serializer<MessageKafka<OrchestratorResponseDTO>> {

  private final ObjectMapper objectMapper = new ObjectMapper();

  @Override
  public byte[] serialize(String topic, MessageKafka data) {
    try {
      return objectMapper.writeValueAsBytes(data);
    } catch (JsonProcessingException e) {
      throw new SerializationException(e);
    }

  }
}
