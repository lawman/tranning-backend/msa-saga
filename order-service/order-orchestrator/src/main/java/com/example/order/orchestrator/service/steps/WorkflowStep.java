package com.example.order.orchestrator.service.steps;

public interface WorkflowStep {
  WorkflowStepStatus getStatus();

  Boolean process();

  Boolean revert();
}
