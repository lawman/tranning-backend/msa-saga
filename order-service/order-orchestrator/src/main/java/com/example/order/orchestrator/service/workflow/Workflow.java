package com.example.order.orchestrator.service.workflow;

import com.example.order.orchestrator.service.steps.WorkflowStep;

import java.util.List;

public interface Workflow {
  List<WorkflowStep> getSteps();
}
