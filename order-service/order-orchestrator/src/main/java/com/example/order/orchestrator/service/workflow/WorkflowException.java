package com.example.order.orchestrator.service.workflow;

public class WorkflowException extends RuntimeException {
  public WorkflowException(String message) {
    super(message);
  }
}
