package com.example.order.orchestrator.service.steps;

public enum WorkflowStepStatus {
  PENDING,
  COMPLETE,
  FAILED;

}
