package com.example.order.orchestrator.service;

import com.example.order.client.dto.OrchestratorRequestDTO;
import com.example.order.client.dto.OrchestratorResponseDTO;

public interface OrchestratorService {
  OrchestratorResponseDTO orderProduct(final OrchestratorRequestDTO requestDTO);
}
