package com.example.order.orchestrator.service.steps.impl;

import com.example.inventory.client.dto.InventoryRequestDTO;
import com.example.inventory.client.dto.InventoryResponseDTO;
import com.example.inventory.client.enums.InventoryStatus;
import com.example.order.orchestrator.routes.InventoryRoute;
import com.example.order.orchestrator.service.steps.WorkflowStep;
import com.example.order.orchestrator.service.steps.WorkflowStepStatus;
import lombok.RequiredArgsConstructor;

@RequiredArgsConstructor
public class InventoryStepImpl implements WorkflowStep {
  private final InventoryRoute inventoryRoute;
  private final InventoryRequestDTO requestDTO;
  private WorkflowStepStatus stepStatus = WorkflowStepStatus.PENDING;


  @Override
  public WorkflowStepStatus getStatus() {
    return this.stepStatus;
  }

  @Override
  public Boolean process() {
    InventoryResponseDTO inventoryResponseDTO = this.inventoryRoute.deduct(this.requestDTO);

    this.stepStatus = inventoryResponseDTO.getStatus().equals(InventoryStatus.AVAILABLE) ? WorkflowStepStatus.COMPLETE : WorkflowStepStatus.FAILED;

    return this.stepStatus.equals(WorkflowStepStatus.COMPLETE);

  }

  @Override
  public Boolean revert() {

    this.inventoryRoute.add(this.requestDTO);
    this.stepStatus = WorkflowStepStatus.FAILED;
    return true;
  }
}
