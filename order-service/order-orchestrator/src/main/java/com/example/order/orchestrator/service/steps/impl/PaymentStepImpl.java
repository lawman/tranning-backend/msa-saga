package com.example.order.orchestrator.service.steps.impl;

import com.example.order.orchestrator.routes.PaymentRoute;
import com.example.order.orchestrator.service.steps.WorkflowStep;
import com.example.order.orchestrator.service.steps.WorkflowStepStatus;
import com.example.payment.client.dto.PaymentRequestDTO;
import com.example.payment.client.dto.PaymentResponseDTO;
import com.example.payment.client.enums.PaymentStatus;
import lombok.RequiredArgsConstructor;

@RequiredArgsConstructor
public class PaymentStepImpl implements WorkflowStep {
  private final PaymentRoute paymentRoute;
  private final PaymentRequestDTO requestDTO;
  private WorkflowStepStatus stepStatus = WorkflowStepStatus.PENDING;


  @Override
  public WorkflowStepStatus getStatus() {
    return this.stepStatus;
  }

  @Override
  public Boolean process() {

    PaymentResponseDTO paymentResponseDTO = this.paymentRoute.debit(this.requestDTO);
    this.stepStatus = paymentResponseDTO.getStatus().equals(PaymentStatus.PAYMENT_APPROVED) ? WorkflowStepStatus.COMPLETE : WorkflowStepStatus.FAILED;

    return this.stepStatus.equals(WorkflowStepStatus.COMPLETE);
  }

  @Override
  public Boolean revert() {
    this.paymentRoute.credit(this.requestDTO);
    this.stepStatus = WorkflowStepStatus.FAILED;
    return true;
  }
}
