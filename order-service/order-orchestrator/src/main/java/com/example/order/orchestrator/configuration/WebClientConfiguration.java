//package com.example.order.orchestrator.configuration;
//
//
//import org.springframework.beans.factory.annotation.Qualifier;
//import org.springframework.context.annotation.Bean;
//import org.springframework.context.annotation.Configuration;
//
//@Configuration
//public class WebClientConfiguration {
//
//
//  @Bean
//  @Qualifier("payment")
//  public WebClient paymentClient(@Value("${service.endpoints.payment}") String endpoint){
//    return WebClient.builder()
//          .baseUrl(endpoint)
//          .build();
//  }
//
//  @Bean
//  @Qualifier("inventory")
//  public WebClient inventoryClient(@Value("${service.endpoints.inventory}") String endpoint){
//    return WebClient.builder()
//          .baseUrl(endpoint)
//          .build();
//  }
//}
