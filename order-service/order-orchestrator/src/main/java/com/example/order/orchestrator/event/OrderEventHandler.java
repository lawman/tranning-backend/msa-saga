package com.example.order.orchestrator.event;

import com.example.order.client.dto.MessageKafka;
import com.example.order.client.dto.OrchestratorRequestDTO;
import com.example.order.client.dto.OrchestratorResponseDTO;
import com.example.order.orchestrator.service.OrchestratorService;
import lombok.RequiredArgsConstructor;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import java.util.function.Function;

@Configuration
@RequiredArgsConstructor
public class OrderEventHandler {
  private final OrchestratorService orchestratorService;

  @Bean
  public Function<MessageKafka<OrchestratorRequestDTO>, MessageKafka<OrchestratorResponseDTO>> processor() {

    return message -> {
      System.out.println("received " + message);
      OrchestratorResponseDTO response = orchestratorService.orderProduct(message.getData());

      return new MessageKafka<>(200, response);
    };
  }
}
