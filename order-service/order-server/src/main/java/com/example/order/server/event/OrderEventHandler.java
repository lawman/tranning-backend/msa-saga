package com.example.order.server.event;

import com.example.order.client.dto.MessageKafka;
import com.example.order.client.dto.OrchestratorResponseDTO;
import com.example.order.server.service.OrderService;
import lombok.RequiredArgsConstructor;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import java.util.function.Consumer;


@Configuration
@RequiredArgsConstructor
public class OrderEventHandler {

  private final OrderService orderService;
//  @Bean
//  public Supplier<MessageKafka> producer() {
//    return () -> new MessageKafka(200, "What's up");
//  }

  @Bean
  public Consumer<MessageKafka<OrchestratorResponseDTO>> consumer() {

    return message -> {
      System.out.println("received " + message);
      orderService.update(message.getData());
    };
  }


}
