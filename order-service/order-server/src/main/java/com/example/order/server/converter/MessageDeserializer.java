package com.example.order.server.converter;

import com.example.order.client.dto.MessageKafka;
import com.example.order.client.dto.OrchestratorRequestDTO;
import com.example.order.client.dto.OrchestratorResponseDTO;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.apache.kafka.common.errors.SerializationException;
import org.apache.kafka.common.serialization.Deserializer;

import java.io.IOException;

public class MessageDeserializer implements Deserializer<MessageKafka<OrchestratorResponseDTO>> {
  private final ObjectMapper objectMapper = new ObjectMapper();

  @Override
  public MessageKafka<OrchestratorResponseDTO> deserialize(String topic, byte[] data) {
    try {
      return objectMapper.readValue(new String(data), new TypeReference<MessageKafka<OrchestratorResponseDTO>>() {
      });
    } catch (IOException e) {
      throw new SerializationException(e);
    }
  }
}
