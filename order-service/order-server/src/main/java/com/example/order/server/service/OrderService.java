package com.example.order.server.service;

import com.example.order.client.dto.OrchestratorResponseDTO;
import com.example.order.client.dto.OrderRequestDTO;
import com.example.order.client.dto.OrderResponseDTO;
import com.example.order.server.entity.Order;

import java.util.List;

public interface OrderService {
  Order create(OrderRequestDTO orderRequestDTO);

  List<OrderResponseDTO> getAll();

  void update(final OrchestratorResponseDTO responseDTO);
}
