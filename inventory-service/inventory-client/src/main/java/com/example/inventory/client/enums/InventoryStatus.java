package com.example.inventory.client.enums;

public enum InventoryStatus {
  AVAILABLE,
  UNAVAILABLE;
}
