package com.example.inventory.server.service.impl;

import com.example.inventory.client.dto.InventoryRequestDTO;
import com.example.inventory.client.dto.InventoryResponseDTO;
import com.example.inventory.client.enums.InventoryStatus;
import com.example.inventory.server.service.InventoryService;
import jakarta.annotation.PostConstruct;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.Map;

@Slf4j
@Service
public class InventoryServiceImpl implements InventoryService {

  private Map<Integer, Integer> productInventoryMap;

  @PostConstruct
  private void init() {
    this.productInventoryMap = new HashMap<>();
    this.productInventoryMap.put(1, 5);
    this.productInventoryMap.put(2, 5);
    this.productInventoryMap.put(3, 5);
  }

  public InventoryResponseDTO deductInventory(final InventoryRequestDTO requestDTO) {
    log.info("Deducting inventory for product id: {}", requestDTO.getProductId());
    int quantity = this.productInventoryMap.getOrDefault(requestDTO.getProductId(), 0);
    InventoryResponseDTO responseDTO = new InventoryResponseDTO();
    responseDTO.setOrderId(requestDTO.getOrderId());
    responseDTO.setUserId(requestDTO.getUserId());
    responseDTO.setProductId(requestDTO.getProductId());
    responseDTO.setStatus(InventoryStatus.UNAVAILABLE);
    if (quantity > 0) {
      responseDTO.setStatus(InventoryStatus.AVAILABLE);
      this.productInventoryMap.put(requestDTO.getProductId(), quantity - 1);
    }
    return responseDTO;
  }

  public void addInventory(final InventoryRequestDTO requestDTO) {
    log.info("Adding inventory for product id: {}", requestDTO.getProductId());
    this.productInventoryMap
          .computeIfPresent(requestDTO.getProductId(), (k, v) -> v + 1);
  }

  @Override
  public Integer getInventory(final Integer productId) {
    return this.productInventoryMap.getOrDefault(productId, 0);
  }
}
