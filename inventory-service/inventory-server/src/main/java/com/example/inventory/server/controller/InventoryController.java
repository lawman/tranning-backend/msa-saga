package com.example.inventory.server.controller;

import com.example.inventory.client.dto.InventoryRequestDTO;
import com.example.inventory.client.dto.InventoryResponseDTO;
import com.example.inventory.server.service.InventoryService;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;


@RestController
@RequestMapping("/api/v1/inventory")
@RequiredArgsConstructor
public class InventoryController {

  private final InventoryService service;

  @PostMapping("/deduct")
  public InventoryResponseDTO deduct(@RequestBody final InventoryRequestDTO requestDTO) {
    return this.service.deductInventory(requestDTO);
  }

  @PostMapping("/add")
  public void add(@RequestBody final InventoryRequestDTO requestDTO) {
    this.service.addInventory(requestDTO);
  }


  @GetMapping("/{productId}")
  public Integer get(@PathVariable final Integer productId) {
    return this.service.getInventory(productId);
  }
}
