package com.example.inventory.server.service;

import com.example.inventory.client.dto.InventoryRequestDTO;
import com.example.inventory.client.dto.InventoryResponseDTO;

public interface InventoryService {
  InventoryResponseDTO deductInventory(final InventoryRequestDTO requestDTO);

  void addInventory(final InventoryRequestDTO requestDTO);

  Integer getInventory(final Integer productId);
}
