package com.example.payment.client.enums;

public enum PaymentStatus {
  PAYMENT_APPROVED,
  PAYMENT_REJECTED;
}
