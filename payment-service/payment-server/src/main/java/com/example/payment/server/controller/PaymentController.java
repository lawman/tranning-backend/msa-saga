package com.example.payment.server.controller;

import com.example.payment.client.dto.PaymentRequestDTO;
import com.example.payment.client.dto.PaymentResponseDTO;
import com.example.payment.server.service.PaymentService;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/api/v1/payments")
@RequiredArgsConstructor
public class PaymentController {
  private final PaymentService service;

  @PostMapping("/debit")
  public PaymentResponseDTO debit(@RequestBody PaymentRequestDTO requestDTO) {
    return this.service.debit(requestDTO);
  }

  @PostMapping("/credit")
  public void credit(@RequestBody PaymentRequestDTO requestDTO) {
    this.service.credit(requestDTO);
  }


  @GetMapping("/{userId}")
  public Double getBalance(@PathVariable final Integer userId) {
    return this.service.getBalance(userId);
  }
}
