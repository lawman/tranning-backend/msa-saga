package com.example.payment.server.service;

import com.example.payment.client.dto.PaymentRequestDTO;
import com.example.payment.client.dto.PaymentResponseDTO;

public interface PaymentService {

  PaymentResponseDTO debit(final PaymentRequestDTO requestDTO);

  void credit(final PaymentRequestDTO requestDTO);

  Double getBalance(final Integer userId);
}
